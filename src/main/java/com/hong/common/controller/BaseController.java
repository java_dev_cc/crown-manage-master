package com.hong.common.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hong.common.AjaxResult;
import com.hong.common.util.BaseEntity;
import com.hong.common.util.ShiroUtil;
import com.hong.entity.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class BaseController {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 获取分页页面
     *
     * @param limit
     * @param offset
     * @return
     */
    protected Integer getPageNo(Integer limit, Integer offset) {
        return offset == 0 ? 1 : offset / limit + 1;
    }

    /**
     * 重定向至地址 url
     *
     * @param url 请求地址
     * @return
     */
    protected String redirectTo(String url) {
        StringBuffer rto = new StringBuffer("redirect:");
        rto.append(url);
        return rto.toString();
    }

    /**
     * 获取登录用户类型
     */
    public Integer getUserType() {
        return getSysUser().getUserType();
    }

    /**
     * 返回 JSON 格式对象
     *
     * @param object 转换对象
     * @return
     */
    protected String toJson(Object object) {
        return JSON.toJSONString(object, SerializerFeature.BrowserCompatible);
    }

    /**
     * 返回 JSON 格式对象
     *
     * @param object 转换对象
     * @param format 序列化特点
     * @return
     */
    protected String toJson(Object object, String format) {
        if (format == null) {
            return toJson(object);
        }
        return JSON.toJSONStringWithDateFormat(object, format, SerializerFeature.WriteDateUseDateFormat);
    }

    /**
     * 获取用户缓存信息
     */
    public SysUser getSysUser() {
        return ShiroUtil.getSessionUser();
    }

    /**
     * 获取登录用户id
     */
    public String getUserId() {
        return getSysUser().getId();
    }


    /**
     * 分页
     */
    public void startPage(BaseEntity baseEntity) {
        PageHelper.startPage(getPageNo(baseEntity.getLimit(), baseEntity.getOffset()), baseEntity.getLimit());
    }

    public AjaxResult getDataTable(List<?> list) {
        PageInfo<?> pageData = new PageInfo(list);
        AjaxResult resultVo = AjaxResult.success();
        resultVo.put("total", pageData.getTotal());
        resultVo.put("rows", pageData.getList());
        return resultVo;
    }


    public AjaxResult toAjax(Object obj) {
        return AjaxResult.success(obj);
    }
}
