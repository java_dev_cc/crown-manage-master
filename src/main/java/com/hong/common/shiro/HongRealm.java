package com.hong.common.shiro;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hong.entity.SysUser;
import com.hong.service.ISysRoleMenuService;
import com.hong.service.ISysUserRoleService;
import com.hong.service.ISysUserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

/**
 * shiro Realm
 */
public class HongRealm extends AuthorizingRealm {

    /**
     * 用户服务
     */
    @Autowired
    private ISysUserService userService;
    /**
     * 用户角色服务
     */
    @Autowired
    private ISysUserRoleService sysUserRoleService;
    /**
     * 角色菜单服务
     */
    @Autowired
    private ISysRoleMenuService sysRoleMenuService;

    /**
     * 认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken user = (UsernamePasswordToken) token;
        SysUser sysUser = userService.getOne(new QueryWrapper<SysUser>().eq("user_name", user.getUsername()));
        if (sysUser == null) {
            throw new UnknownAccountException();
        }
        if (sysUser.getUserState() == SysUser._0) {
            throw new LockedAccountException();
        }
        if (sysUser.getUserState() == SysUser._2) {
            throw new UnknownAccountException();
        }
        // 盐值加密
        ByteSource byteSource = ByteSource.Util.bytes(user.getUsername());
        String password = sysUser.getPassword();
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(sysUser, password, byteSource, getName());
        return info;
    }

    /**
     * 授权
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SysUser sysUser = (SysUser) principals.getPrimaryPrincipal();
        // 角色列表
        Set<String> roles = new HashSet<String>();
        // 功能列表
        Set<String> menus = new HashSet<String>();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        // 管理员拥有所有权限
        if (sysUser.isAdmin()) {
            info.addRole("admin");
            info.addStringPermission("*");
        } else {
            roles = sysUserRoleService.findRolesByUid(sysUser.getId());
            menus = sysRoleMenuService.findMenusByUid(sysUser.getId());
            // 角色加入AuthorizationInfo认证对象
            info.setRoles(roles);
            // 权限加入AuthorizationInfo认证对象
            info.setStringPermissions(menus);
        }
        return info;
    }

    /**
     * 清理指定用户授权信息缓存
     */
    public void clearCachedAuthorizationInfo(Object principal) {
        SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
        this.clearCachedAuthorizationInfo(principals);
    }

    /**
     * 清理所有用户授权信息缓存
     */
    public void clearAllCachedAuthorizationInfo() {
        Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
        if (cache != null) {
            for (Object key : cache.keys()) {
                cache.remove(key);
            }
        }
    }

    /**
     * 密码加密
     */
    public static void main(String[] args) {
        // MD5,"原密码","盐",加密次数
        String pwd = new SimpleHash("MD5", "admin123", "admin", 1024).toString();
        System.out.println(pwd);
    }
}
