package com.hong.common.util;

import com.hong.entity.SysUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * @description:系统工具
 * @author：cuic
 * @date：2022/3/10 11:01
 */
public class SystemUtils {


    /***
     * 获取当前登录用户
     * **/
    public static SysUser getCurrentUser() {
        // 记录登录日志
        Subject subject = SecurityUtils.getSubject();
        return null == subject ? null : (SysUser) subject.getPrincipal();
    }

}
