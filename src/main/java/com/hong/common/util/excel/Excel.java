package com.hong.common.util.excel;


/**
 * Excel 单元格数据信息
 *
 * @author 崔晨
 * 最后更新日期：2020-08-23
 */

public class Excel {

    /**
     * 值
     */
    private Object value = "";
    /**
     * 批注
     */
    private String comment;
    /**
     * 表格宽度
     */
    private Integer width = 15;
    /**
     * 是否表头（上下左右相邻且内容一致的表头将会自动合并）
     */
    private boolean isHead = false;

    /**
     * 样式  1加粗 2居中 3灰色背景 4边框 5红色字体  12加粗居中  24居中带边框 123加粗居中灰色背景 1234加粗居中灰色背景带边框
     */
    private ExcelStyleEnum style;


    public Excel() {
        super();
        value = "";
    }


    public Excel(Object value) {
        super();
        this.value = value;
    }

    public Excel(Object value, ExcelStyleEnum style) {
        super();
        this.value = value;
        this.style = style;
    }

    public Excel(Object value, int width) {
        super();
        this.value = value;
        this.width = width;

    }

    public Excel(Object value, int width, ExcelStyleEnum style) {
        super();
        this.value = value;
        this.width = width;
        this.style = style;
    }

    public Excel(boolean isHead, Object value) {
        super();
        this.value = value;
        this.isHead = isHead;
        this.style = style;
    }

    public Excel(boolean isHead, Object value, ExcelStyleEnum style) {
        super();
        this.value = value;
        this.isHead = isHead;
        this.style = style;
    }

    public Excel(boolean isHead, Object value, int width) {
        super();
        this.value = value;
        this.isHead = isHead;
        this.width = width;
        this.style = style;
    }

    public Excel(boolean isHead, Object value, int width, ExcelStyleEnum style) {
        super();
        this.value = value;
        this.isHead = isHead;
        this.width = width;
        this.style = style;
    }


    public static Excel valueOf(Object value) {
        return new Excel(value);
    }

    public Object getValue() {

        if (null != value) {
            return value;
        } else {
            return "";
        }
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public boolean isHead() {
        return isHead;
    }

    public Excel setHead(boolean head) {
        isHead = head;
        return this;
    }


    public ExcelStyleEnum getStyle() {
        return style;
    }

    public void setStyle(ExcelStyleEnum style) {
        this.style = style;
    }
}
