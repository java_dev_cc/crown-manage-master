package com.hong.common.util.excel;

/**
 * Excel 合并单元格
 *
 * @author 崔晨
 * 最后更新日期：2020-08-23
 */
public class ExcelRange {

    /*
     * 表头名称
     * */
    private String title;
    /*
     * 行数
     * */
    private int rowNum;

    /*
     * 列数
     * */
    private int rankNum;

    /*
     * 结尾行数
     * */
    private int endRowNum;

    /*
     * 结尾列数
     * */
    private int endRankNum;


    public ExcelRange() {

    }

    public ExcelRange(String title, int rowNum, int rankNum) {
        this.title = title;
        this.rowNum = rowNum;
        this.rankNum = rankNum;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public int getRankNum() {
        return rankNum;
    }

    public void setRankNum(int rankNum) {
        this.rankNum = rankNum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getEndRowNum() {
        return endRowNum;
    }

    public void setEndRowNum(int endRowNum) {
        this.endRowNum = endRowNum;
    }

    public int getEndRankNum() {
        return endRankNum;
    }

    public void setEndRankNum(int endRankNum) {
        this.endRankNum = endRankNum;
    }
}
