package com.hong.common.util.excel;


import java.util.List;
import java.util.Map;

/**
 * Excel Sheet表单
 *
 * @author 崔晨
 * 最后更新日期：2020-08-23
 */

public class ExcelSheet {

    /**
     * Sheet名称
     */
    private String name;

    /**
     * 表头名称
     */
    private String headName;

    /**
     * 表头所占行数
     */
    private int rowNum;

    /**
     * 表单数据
     */
    private List<Map<String, Excel>> dataList;


    public ExcelSheet() {

    }

    public ExcelSheet(List<Map<String, Excel>> dataList) {
        this.dataList = dataList;
    }

    public ExcelSheet(List<Map<String, Excel>> dataList, String name) {
        this.dataList = dataList;
        this.name = name;
    }

    public ExcelSheet(List<Map<String, Excel>> dataList, String headName, int rowNum) {
        this.dataList = dataList;
        this.headName = headName;
        this.rowNum = rowNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeadName() {
        return headName;
    }

    public void setHeadName(String headName) {
        this.headName = headName;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public List<Map<String, Excel>> getDataList() {
        return dataList;
    }

    public void setDataList(List<Map<String, Excel>> dataList) {
        this.dataList = dataList;
    }
}
