package com.hong.common.util.excel;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

/**
 * Excel单元格样式
 * 需要新样式，直接在里面添加即可
 *
 * @author 崔晨
 * 最后更新日期：2020-08-23
 */
public class ExcelStyle {


    // 1234加粗居中灰色背景带边框
    private CellStyle centerBoldFrameGrayBack;

    // 1234加粗居中灰色背景带边框
    private CellStyle centerBoldGrayBack;

    // 24居中带边框
    private CellStyle centerFrame;

    // 1加粗
    private CellStyle bold;

    // 12加粗居中
    private CellStyle boldCenter;

    // 创建默认单元格样式
    private CellStyle cellStyle;

    // 12加粗居中 创建垂直水平居中表头样式
    private CellStyle centerStyle;


    public ExcelStyle() {

    }

    public ExcelStyle(HSSFWorkbook workbook) {

        //加粗左右垂直居中灰色背景样式
        centerBoldFrameGrayBack = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBold(true);// 粗体显示
        centerBoldFrameGrayBack.setFont(font);
        centerBoldFrameGrayBack.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        centerBoldFrameGrayBack.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        //设置水平对齐的样式为居中对齐;
        centerBoldFrameGrayBack.setAlignment(HorizontalAlignment.CENTER);
        //设置垂直对齐的样式为居中对齐;
        centerBoldFrameGrayBack.setVerticalAlignment(VerticalAlignment.CENTER);
        centerBoldFrameGrayBack.setBorderBottom(BorderStyle.THIN);
        centerBoldFrameGrayBack.setBorderLeft(BorderStyle.THIN);
        centerBoldFrameGrayBack.setBorderTop(BorderStyle.THIN);
        centerBoldFrameGrayBack.setBorderRight(BorderStyle.THIN);


        //加粗左右垂直居中灰色背景样式
        centerBoldGrayBack = workbook.createCellStyle();
        centerBoldGrayBack.setFont(font);
        centerBoldGrayBack.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        centerBoldGrayBack.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        //设置水平对齐的样式为居中对齐;
        centerBoldGrayBack.setAlignment(HorizontalAlignment.CENTER);
        //设置垂直对齐的样式为居中对齐;
        centerBoldGrayBack.setVerticalAlignment(VerticalAlignment.CENTER);
        centerBoldGrayBack.setBorderBottom(BorderStyle.THIN);
        centerBoldGrayBack.setBorderLeft(BorderStyle.THIN);
        centerBoldGrayBack.setBorderTop(BorderStyle.THIN);
        centerBoldGrayBack.setBorderRight(BorderStyle.THIN);


        //单元格样式
        cellStyle = workbook.createCellStyle();
        //设置水平对齐的样式为居中对齐;
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        //设置垂直对齐的样式为居中对齐;
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        //自动换行以展示全部内容
        cellStyle.setWrapText(true);

        //加粗左右垂直居中样式
        centerStyle = workbook.createCellStyle();
        HSSFFont centerFont = workbook.createFont();
        centerFont.setBold(true);// 粗体显示
        centerStyle.setFont(centerFont);
        //设置水平对齐的样式为居中对齐;
        centerStyle.setAlignment(HorizontalAlignment.CENTER);
        //设置垂直对齐的样式为居中对齐;
        centerStyle.setVerticalAlignment(VerticalAlignment.CENTER);


        //单元格样式
        centerFrame = workbook.createCellStyle();
        //设置水平对齐的样式为居中对齐;
        centerFrame.setAlignment(HorizontalAlignment.CENTER);
        //设置垂直对齐的样式为居中对齐;
        centerFrame.setVerticalAlignment(VerticalAlignment.CENTER);
        //自动换行以展示全部内容
        centerFrame.setWrapText(true);
        centerFrame.setBorderBottom(BorderStyle.THIN);
        centerFrame.setBorderLeft(BorderStyle.THIN);
        centerFrame.setBorderTop(BorderStyle.THIN);
        centerFrame.setBorderRight(BorderStyle.THIN);


        //加粗左右垂直居中样式
        bold = workbook.createCellStyle();
        HSSFFont boldFont = workbook.createFont();
        boldFont.setBold(true);// 粗体显示
        bold.setFont(boldFont);

        boldCenter = workbook.createCellStyle();
        boldCenter.setFont(boldFont);
        //设置水平对齐的样式为居中对齐;
        boldCenter.setAlignment(HorizontalAlignment.CENTER);
        //设置垂直对齐的样式为居中对齐;
        boldCenter.setVerticalAlignment(VerticalAlignment.CENTER);

    }


    public CellStyle getCenterBoldFrameGrayBack() {
        return centerBoldFrameGrayBack;
    }

    public void setCenterBoldFrameGrayBack(CellStyle centerBoldFrameGrayBack) {
        this.centerBoldFrameGrayBack = centerBoldFrameGrayBack;
    }

    public CellStyle getCenterBoldGrayBack() {
        return centerBoldGrayBack;
    }

    public void setCenterBoldGrayBack(CellStyle centerBoldGrayBack) {
        this.centerBoldGrayBack = centerBoldGrayBack;
    }

    public CellStyle getCenterFrame() {
        return centerFrame;
    }

    public void setCenterFrame(CellStyle centerFrame) {
        this.centerFrame = centerFrame;
    }

    public CellStyle getBold() {
        return bold;
    }

    public void setBold(CellStyle bold) {
        this.bold = bold;
    }

    public CellStyle getBoldCenter() {
        return boldCenter;
    }

    public void setBoldCenter(CellStyle boldCenter) {
        this.boldCenter = boldCenter;
    }

    public CellStyle getCellStyle() {
        return cellStyle;
    }

    public void setCellStyle(CellStyle cellStyle) {
        this.cellStyle = cellStyle;
    }

    public CellStyle getCenterStyle() {
        return centerStyle;
    }

    public void setCenterStyle(CellStyle centerStyle) {
        this.centerStyle = centerStyle;
    }
}
