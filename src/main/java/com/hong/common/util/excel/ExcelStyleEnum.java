package com.hong.common.util.excel;


import java.util.HashMap;
import java.util.Map;

/**
 * @author 崔晨
 * 最后更新日期：2020-08-23
 */

public enum ExcelStyleEnum {


    /**
     * 1234加粗居中灰色背景带边框
     */
    CENTER_BOLD_FRAME_GRAY_BACK(1234),

    /***
     * 12居中加粗
     * */
    BOLD_CENTER(12),
    /***
     * 24居中带边框
     * */
    CENTER_FRAME(24),

    /***
     * 1加粗
     * */
    BOLD(1);

    private final Integer code;

    private ExcelStyleEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    private static final Map<Integer, ExcelStyleEnum> _data = new HashMap<>();

    static {
        for (ExcelStyleEnum c : values()) {
            _data.put(c.getCode(), c);
        }
    }

    public static ExcelStyleEnum valueOf(Integer code) {
        return _data.get(code);
    }
}
