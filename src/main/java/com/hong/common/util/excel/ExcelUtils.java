package com.hong.common.util.excel;


import com.hong.common.util.DateUtils;
import com.hong.common.util.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;


/**
 * Excel处理工具
 * 支持复杂表头
 * 支持批注
 * 支持指定样式
 * 支持自定义宽度
 * 支持多个Sheet表单同时导出
 * 支持设置高亮
 *
 * @author 崔晨
 * 最后更新日期：2020-08-23
 */
public class ExcelUtils {


    /**
     * 格式化单元格返回其内容 格式化成string返回。
     *
     * @param cell
     * @return
     */
    public static String formatCell(Cell cell) {
        String cellValue = null;
        if (cell != null) {
            // 判断cell类型

            switch (cell.getCellType()) {
                case NUMERIC: {
                    cellValue = String.valueOf(cell.getNumericCellValue());
                    break;
                }
                case STRING: {
                    cellValue = cell.getRichStringCellValue().getString();
                    break;
                }
                default:
                    cellValue = "";
            }
        } else {
            cellValue = "";
        }
        return cellValue.trim();
    }

    /**
     * @param response
     * @param wb       返回文件
     * @param fileName
     * @throws Exception
     */
    public static void export(HttpServletResponse response, Workbook wb, String fileName) throws Exception {

        // 避免命名不规范
        if (null == fileName) {
            fileName = DateUtils.dateTime() + ".xls";
        } else {
            if (fileName.indexOf(".xls") == -1) {
                fileName = fileName + "__" + DateUtils.dateTime() + ".xls";
            } else {
                fileName = fileName.replace(".xls", "") + "__" + DateUtils.dateTime() + ".xls";
            }
        }
        response.setHeader("Content-Disposition",
                "attachment;filename=" + new String(fileName.getBytes("utf-8"), "iso8859-1"));
        response.setContentType("application/ynd.ms-excel;charset=UTF-8");
        OutputStream out = response.getOutputStream();
        wb.write(out);

        out.flush();
        out.close();
    }


    /**
     * @param workbook 返回文件
     * @param fileName
     * @throws Exception
     */
    public static void export(Workbook workbook, String fileName) throws Exception {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = Objects.requireNonNull(servletRequestAttributes).getResponse();
        export(response, workbook, fileName);
    }

    /**
     * 把集合转成excel导出
     *
     * @param dataList
     * @throws Exception
     */
    public static HSSFWorkbook listToExcel(List<ExcelSheet> dataList) throws Exception {
        if (null != dataList && dataList.size() > 0) {
            // 创建Excel文档
            HSSFWorkbook workbook = new HSSFWorkbook();
            // 遍历表单
            int sheetNum = 1;
            for (ExcelSheet sheet : dataList) {
                List<Map<String, Excel>> data = sheet.getDataList();

                String sheetName = null == sheet.getName() ? "Sheet" + sheetNum++ : sheet.getName();


                HSSFSheet excelSheet = workbook.createSheet(sheetName);

                //表头不为空
                if (StringUtils.isNotEmpty(sheet.getHeadName()) && !CollectionUtils.isEmpty(data)) {
                    //表格样式
                    ExcelStyle excelStyle = new ExcelStyle(workbook);
                    HSSFRow row = excelSheet.createRow(0);
                    HSSFCell cell = row.createCell(0);
                    cell.setCellValue((sheet.getHeadName()));
                    cell.setCellStyle(excelStyle.getCenterStyle());
                    CellRangeAddress region = new CellRangeAddress(0, sheet.getRowNum() - 1, 0, data.get(0).size() - 1);
                    excelSheet.addMergedRegion(region);
                }

                // 顺序 工作簿,表单,表单的数据
                getSheet(workbook, excelSheet, data, sheet.getRowNum());
            }
            return workbook;
        }
        return null;

    }

    /**
     * 获取单个sheet表单
     *
     * @param sheet
     * @param sheetDate
     */
    public static HSSFSheet getSheet(HSSFWorkbook workbook, HSSFSheet sheet, List<Map<String, Excel>> sheetDate, int rowNum) {

        if (null != sheetDate && sheetDate.size() > 0) {
            Map<String, Excel> headMap = sheetDate.get(0);
            List<ExcelRange> titleLists = new ArrayList<>();
            // 获取所有的key
            Set<String> keys = headMap.keySet();
            // 为了保证顺序，转为List
            List<String> ranks = new ArrayList<String>(keys);
            //表格样式
            ExcelStyle excelStyle = new ExcelStyle(workbook);


            //首先判断是否是普通表头，否则按复杂表头进行处理
            Excel keyExcel = headMap.get(ranks.get(0)) == null ? new Excel() : headMap.get(ranks.get(0));
            if (!keyExcel.isHead()) {
                // 5.设置表头
                HSSFRow headerRow = sheet.createRow(rowNum++);
                // 增加表头字段
                for (int i = 0; i < ranks.size(); i++) {
                    Excel head = headMap.get(ranks.get(i)) == null ? new Excel() : headMap.get(ranks.get(0));
                    HSSFCell cell = headerRow.createCell(i);
                    sheet.setColumnWidth(i, head.getWidth() * 256);
                    cell.setCellValue(ranks.get(i));
                    // 样式
                    cell.setCellStyle(excelStyle.getCenterBoldGrayBack());
                }
            } else {
                //复杂表头则不生成普通表头,只设置列宽
                for (int i = 0; i < ranks.size(); i++) {
                    Excel head = headMap.get(ranks.get(i));
                    sheet.setColumnWidth(i, head.getWidth() * 256);
                }
            }
            // 增加批注对象
            HSSFPatriarch p = sheet.createDrawingPatriarch();
            // 遍历每行元素
            for (Map<String, Excel> rowMap : sheetDate) {
                // 创建行
                HSSFRow row = sheet.createRow(rowNum++);
                int cellNum = 0;
                for (String key : ranks) {
                    // 获取该行参数
                    Excel cellParam = rowMap.get(key) == null ? new Excel() : rowMap.get(key);
                    // 创建单元格
                    HSSFCell cell = row.createCell(cellNum++);
                    String value = null != cellParam && null != cellParam.getValue() ? cellParam.getValue().toString() : "";
                    if (null != cellParam) {
                        if (cellParam.isHead()) {
                            //合并等表头类操作稍后处理
                            titleLists.add(new ExcelRange(value, rowNum - 1, cellNum - 1));
                        }
                        // 单元格值
                        cell.setCellValue(value);

                        setCellStyle(cell, excelStyle, cellParam.getStyle());
                        if (null != cellParam.getComment() && !"".equals(cellParam.getComment().trim())) {
                            HSSFComment comment = p
                                    .createCellComment(new HSSFClientAnchor(0, 0, 0, 0, (short) 3, 3, (short) 9, 7));
                            // 设置批注
                            comment.setString(new HSSFRichTextString(cellParam.getComment()));
                            // 写入批注内容
                            cell.setCellComment(comment);

                        }
                    }
                }

            }
            //合并单元格后返回
            return RangeCell(sheet, titleLists);

        }

        return sheet;

    }

    private static void setCellStyle(HSSFCell cell, ExcelStyle excelStyle, ExcelStyleEnum style) {
        if (ExcelStyleEnum.CENTER_BOLD_FRAME_GRAY_BACK.equals(style)) {
            cell.setCellStyle(excelStyle.getCenterBoldFrameGrayBack());
        }

        if (ExcelStyleEnum.CENTER_FRAME.equals(style)) {
            cell.setCellStyle(excelStyle.getCenterFrame());
        }

        if (ExcelStyleEnum.BOLD.equals(style)) {
            cell.setCellStyle(excelStyle.getBold());
        }
        if (ExcelStyleEnum.BOLD_CENTER.equals(style)) {
            cell.setCellStyle(excelStyle.getBoldCenter());
        }


    }

    /**
     * 合并周围相同的单元格
     */
    private static HSSFSheet RangeCell(HSSFSheet sheet, List<ExcelRange> titleLists) {
        List<ExcelRange> excelRangeList = getRangeCell(titleLists);
        if (null != excelRangeList && excelRangeList.size() > 0) {
            for (ExcelRange excelRange : excelRangeList) {
                //合并单元格
                CellRangeAddress region = new CellRangeAddress(excelRange.getRowNum(), excelRange.getEndRowNum(), excelRange.getRankNum(), excelRange.getEndRankNum());
                sheet.addMergedRegion(region);
            }
        }
        return sheet;
    }

    /**
     * 获取需合并的单元格
     */
    private static List<ExcelRange> getRangeCell(List<ExcelRange> excelRanges) {

        //数量大于1才有合并的必要
        if (null != excelRanges && excelRanges.size() > 1) {
            List<ExcelRange> realExcelRange = new ArrayList<>();
            for (int index = 0; index < excelRanges.size(); index++) {
                ExcelRange excelRange = excelRanges.get(index);
                if (!isRangeScope(realExcelRange, excelRange)) {
                    ExcelRange endRange = getEndRangeCell(excelRange, excelRange, excelRanges, index + 1);
                    if (endRange.getRowNum() > excelRange.getRowNum() || endRange.getRankNum() > excelRange.getRankNum()) {
                        excelRange.setEndRowNum(endRange.getRowNum());
                        excelRange.setEndRankNum(endRange.getRankNum());
                        realExcelRange.add(excelRange);
                    }
                }

            }

            return realExcelRange;
        }
        return null;
    }

    /**
     * 判断当前单元格是否已经在合并范围内
     */
    private static boolean isRangeScope(List<ExcelRange> realExcelRange, ExcelRange excelRange) {

        if (null != realExcelRange && realExcelRange.size() > 0) {
            for (ExcelRange scope : realExcelRange) {
                if (scope.getEndRowNum() >= excelRange.getRowNum() && scope.getRowNum() <= excelRange.getRowNum()
                        && scope.getEndRankNum() >= excelRange.getRankNum() && scope.getRankNum() <= excelRange.getRankNum()
                ) {
                    //行数介于合并格之间且列数介于合并格之间表示属于已合并的内容
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取合并单元格的封口节点
     */
    private static ExcelRange getEndRangeCell(ExcelRange startRange, ExcelRange thisRange, List<ExcelRange> excelRanges, int index) {
        if (null != excelRanges && excelRanges.size() > index) {
            for (int i = index; i < excelRanges.size(); i++) {
                ExcelRange nextRange = excelRanges.get(i);
                if (thisRange.getTitle().equals(nextRange.getTitle()) && nextRange.getRowNum() == thisRange.getRowNum() && nextRange.getRankNum() - 1 == thisRange.getRankNum()) {
                    //同行且为右侧列
                    return getEndRangeCell(startRange, nextRange, excelRanges, ++index);
                } else if (nextRange.getRowNum() - 1 == thisRange.getRowNum() && nextRange.getRankNum() >= startRange.getRankNum() && nextRange.getRankNum() <= thisRange.getRankNum()) {
                    if (nextRange.getTitle().equals(startRange.getTitle())) {
                        if (nextRange.getRankNum() == thisRange.getRankNum()) {
                            //找到新的封口节点，继续寻找下一个
                            return getEndRangeCell(startRange, nextRange, excelRanges, ++index);
                        } else {
                            //满足封口条件，继续寻找封口节点
                            return getEndRangeCell(startRange, thisRange, excelRanges, ++index);
                        }
                    } else {
                        //表格无法封口
                        return thisRange;
                    }

                }
            }
        }
        return thisRange;
    }


    /**
     * 把集合转成excel导出
     *
     * @param sheet
     * @return
     * @throws Exception
     */
    public static HSSFWorkbook listToExcelAlone(ExcelSheet sheet) throws Exception {
        List<ExcelSheet> list = new ArrayList<>();
        list.add(sheet);
        return listToExcel(list);

    }

    /**
     * 把集合转成excel导出
     *
     * @param excelList
     * @return
     * @throws Exception
     */
    public static HSSFWorkbook listToExcelAlone(List<Map<String, Excel>> excelList) throws Exception {
        return listToExcelAlone(new ExcelSheet(excelList));

    }

    /**
     * 把集合转成excel导出
     *
     * @param excelList
     * @return
     * @throws Exception
     */
    public static HSSFWorkbook listToExcelAlone(List<Map<String, Excel>> excelList, String headName, int rowNum) throws Exception {
        return listToExcelAlone(new ExcelSheet(excelList, headName, rowNum));

    }

    /**
     * 把集合转成excel导出
     *
     * @param data
     * @return
     * @throws
     */
    public static void listToExcelAlone(List<Map<String, Excel>> data, HttpServletResponse response, String name) throws Exception {
        export(response, listToExcelAlone(new ExcelSheet(data)), name);
    }

    /**
     * 把EXCEl文件转换成集合
     *
     * @param file
     * @return
     * @throws Exception
     */
    public static List<Map<String, String>> excelToArrayList(MultipartFile file)
            throws IllegalStateException, IOException {
        // 输入流
        InputStream inputStream = file.getInputStream();
        try {
            //  excel工作簿
            Workbook wb;
            try {
                wb = new HSSFWorkbook(inputStream);
            } catch (IOException e) {
                wb = new XSSFWorkbook(inputStream);
            }
            // 获取第一个sheet页
            Sheet sheet = wb.getSheetAt(0);
            if (sheet != null) {
                List<String> heads = new ArrayList<>();
                Row rows = sheet.getRow(0);
                int j = 0;
                // 获取抬头字段
                while (true) {
                    String rowValue = ExcelUtils.formatCell(rows.getCell(j++));
                    if (null != rowValue && !"".equals(rowValue)) {
                        heads.add(rowValue);
                    } else {
                        break;
                    }
                }
                List<Map<String, String>> dataList = new ArrayList<>();
                // 从第一行开始(表格的第二行2)
                for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
                    Row row = sheet.getRow(rowNum);
                    if (row == null || StringUtils.isEmpty(ExcelUtils.getValue(row, 0))) {
                        continue;
                    }

                    Map<String, String> data = new HashMap<>();
                    for (int i = 0; i < heads.size(); i++) {
                        data.put(heads.get(i), ExcelUtils.getValue(row, i));
                    }
                    dataList.add(data);
                }

                return dataList;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != inputStream) {
                inputStream.close();
            }
        }
        return null;

    }

    /**
     * 获取EXCEl单元格的值
     *
     * @param row
     * @param index
     * @return
     * @throws Exception
     */
    public static String getValue(Row row, Integer index) {
        if (null != index) {
            return formatCell(row.getCell(index));
        } else {
            return null;
        }
    }
}
