package com.hong.controller;

import com.hong.common.AjaxResult;
import com.hong.common.util.StringUtils;
import com.hong.common.util.excel.Excel;
import com.hong.common.util.excel.ExcelStyleEnum;
import com.hong.common.util.excel.ExcelUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @description:excel通用控制器
 * @author：cuic
 * @date：2022/3/3 16:42
 */
@Controller
@RequestMapping("/system/excel")
public class ExcelController {


    @PostMapping("/importTemplate")
    public AjaxResult importTemplate(String columns) throws Exception {
        if (!StringUtils.isEmpty(columns)) {
            List<Map<String, Excel>> excelDate = new ArrayList<>();

            Map<String, Excel> row = new LinkedHashMap<>();
            for (String column : columns.split(",")) {
                //封装excel格式
                row.put(column, new Excel("", ExcelStyleEnum.CENTER_FRAME));
            }
            excelDate.add(row);
            HSSFWorkbook sheets = ExcelUtils.listToExcelAlone(excelDate);
            ExcelUtils.export(sheets, "excelTemplate.xls");
        }
        return AjaxResult.error();
    }

}


