package com.hong.controller;

import com.hong.common.controller.BaseController;
import com.hong.common.util.SnowflakeIdWorker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * 文件上传控制器
 */

@Controller
public class FileUploadController extends BaseController {

    /**
     * 上传文件
     *
     * @param importFile
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping("/file/upload")
    public Map<String, Object> fileUpload(@RequestParam MultipartFile[] importFile, HttpServletRequest request)
            throws IOException {

        List<String> urls = new ArrayList<String>();
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            for (MultipartFile myfile : importFile) {
                if (myfile.isEmpty()) {
                    logger.warn("文件未上传");
                } else {
                    logger.debug("文件长度: " + myfile.getSize());
                    logger.debug("文件类型: " + myfile.getContentType());
                    logger.debug("文件名称: " + myfile.getName());
                    logger.debug("文件原名: " + myfile.getOriginalFilename());
                    String ext = FilenameUtils.getExtension(myfile.getOriginalFilename());
                    String reName = SnowflakeIdWorker.generateId() + "." + ext;
                    String cdate = DateFormatUtils.format(new Date(), "yyyy-MM-dd");
                    String uploadPath = request.getSession().getServletContext().getRealPath("/upload");
                    String realPath = uploadPath + File.separator + cdate;
                    FileUtils.copyInputStreamToFile(myfile.getInputStream(), new File(realPath, reName));
                    urls.add("/upload/" + cdate + "/" + reName);
                }
            }
            result.put("status", "success");
            result.put("urls", urls);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            result.put("status", "error");
            return result;
        }
    }
}
