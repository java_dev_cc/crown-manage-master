package com.hong.controller;

import com.hong.common.controller.BaseController;
import com.hong.common.util.ShiroUtil;
import com.hong.entity.SysNotice;
import com.hong.entity.SysUser;
import com.hong.entity.vo.TreeMenu;
import com.hong.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class IndexController extends BaseController {

    @Autowired
    private ISysMenuService sysMenuService;
    @Autowired
    private ISysNoticeService sysNoticeService;


    /**
     * 首页内容跳转
     *
     * @return
     */
    @RequestMapping("/index")
    public String index(Model model) {
        // 保存登录信息
        SysUser currentUser = ShiroUtil.getSessionUser();
        currentUser.setPassword("");
        ShiroUtil.setSessionAttribute("currentUser", currentUser);
        // 获取当前用户的菜单
        List<TreeMenu> treeMenus = sysMenuService.selectTreeMenuByUserId(currentUser.getId());
        ShiroUtil.setSessionAttribute("treeMenus", treeMenus);
        List<SysNotice> sysNotices = sysNoticeService.selectSysNoticeList(null);
        ShiroUtil.setSessionAttribute("sysNotices", sysNotices);
        return "index";
    }


    /**
     * 首页内容跳转
     *
     * @return
     */
    @RequestMapping("/favicon.ico")
    public String favicon() {
        return "redirect:/image/favicon.ico";
    }

    /**
     * 无权限跳转
     *
     * @return
     */
    @RequestMapping("/unauth")
    public String unauth() {
        return "error/403";
    }
}
