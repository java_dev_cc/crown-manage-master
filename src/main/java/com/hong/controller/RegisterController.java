package com.hong.controller;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.hong.common.AjaxResult;
import com.hong.common.anno.Log;
import com.hong.common.controller.BaseController;
import com.hong.common.util.ShiroUtil;
import com.hong.common.util.StringUtils;
import com.hong.entity.SysUser;
import com.hong.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/register")
public class RegisterController extends BaseController {

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private Producer captchaProducer;

    /**
     * 登录页面
     *
     * @return
     */
    @RequestMapping
    public String login() {
        return "register";
    }


    @Log("注册")
    @PostMapping("/doRegister")
    @ResponseBody
    public AjaxResult addSave(String userDesc, String userName, String password, String captcha) {
        String rightCode = (String) ShiroUtil.getSessionAttribute(Constants.KAPTCHA_SESSION_KEY);
        if (!(StringUtils.isNotBlank(captcha) && captcha.equals(rightCode))) {
            return AjaxResult.error("验证码错误！");
        }
        int countUserName = sysUserService.selectCountByUserNameAndId(userName, null);
        if (countUserName > 0) {
            return AjaxResult.error("登录账号已存在");
        }

        int countUserDesc = sysUserService.selectCountByUserDescAndId(userDesc, null);
        if (countUserDesc > 0) {
            return AjaxResult.error("用户名称已存在");
        }
        ShiroUtil.removeSessionAttribute(Constants.KAPTCHA_SESSION_KEY);
        SysUser user = new SysUser();
        user.setPassword(password);
        user.setUserDesc(userDesc);
        user.setUserName(userName);
        sysUserService.insertUser(user, "1");

        return toAjax(1);
    }
}
