package com.hong.controller.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hong.common.AjaxResult;
import com.hong.common.anno.Log;
import com.hong.common.controller.BaseController;
import com.hong.common.shiro.AuthorizationUtils;
import com.hong.entity.SysMenu;
import com.hong.service.ISysMenuService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/system/menu")
public class MenuController extends BaseController {

    @Autowired
    private ISysMenuService sysMenuService;

    /**
     * 跳转菜单页
     *
     * @return
     */
    @RequiresPermissions("listMenu")
    @RequestMapping("/listPage")
    public String listPage() {
        return "system/menu/list";
    }

    /**
     * 分页查询菜单
     *
     * @return
     */
    @RequiresPermissions("listMenu")
    @RequestMapping("/listMenu")
    @ResponseBody
    public List<SysMenu> listMenu() {
        List<SysMenu> listMenu = sysMenuService.list();
        for (SysMenu menu : listMenu) {
            if (menu.getPid() == null || menu.getDeep() != 3) {
                menu.setMenuName(StringUtils.join("<i class='" + menu.getIcon() + "'></i> ", menu.getMenuName()));
            } else {
                menu.setMenuName(StringUtils.join("<i class='fa fa-file'></i> ", menu.getMenuName()));
            }
        }
        return listMenu;
    }

    /**
     * 选取图标
     *
     * @return
     */
    @RequestMapping("/toicon")
    public String toicon() {
        return "system/menu/icon";
    }

    /**
     * 创建菜单
     *
     * @param model
     * @return
     */
    @RequiresPermissions("addMenu")
    @RequestMapping("/add")
    public String add(Model model) {
        QueryWrapper<SysMenu> ew = new QueryWrapper<SysMenu>();
        ew.eq("pid", "0");
        ew.orderByAsc("sort");
        List<SysMenu> list = sysMenuService.list(ew);
        model.addAttribute("list", list);
        return "system/menu/add";
    }

    /**
     * 创建目录
     *
     * @param sysMenu
     * @return
     */
    @Log("创建目录菜单")
    @RequiresPermissions("addMenu")
    @RequestMapping("/doAddDir")
    @ResponseBody
    public AjaxResult doAddDir(SysMenu sysMenu) {
        sysMenu.setPid("0");
        sysMenu.setDeep(1);
        sysMenuService.save(sysMenu);
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return AjaxResult.success();
    }

    /**
     * 创建菜单
     *
     * @param sysMenu
     * @param model
     * @return
     */
    @Log("创建菜单")
    @RequiresPermissions("addMenu")
    @RequestMapping("/doAddMenu")
    @ResponseBody
    public AjaxResult doAddMenu(SysMenu sysMenu) {
        sysMenu.setDeep(2);
        sysMenuService.save(sysMenu);
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return AjaxResult.success();
    }

    /**
     * 创建按钮
     *
     * @param sysMenu
     * @param model
     * @return
     */
    @Log("新增按钮")
    @RequiresPermissions("addMenu")
    @RequestMapping("/doAddAction")
    @ResponseBody
    public AjaxResult doAddAction(SysMenu sysMenu) {
        sysMenu.setDeep(3);
        sysMenuService.save(sysMenu);
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return AjaxResult.success();
    }

    /**
     * 编辑菜单
     *
     * @param id
     * @param model
     * @return
     */
    @RequiresPermissions("editMenu")
    @RequestMapping("/edit/{id}")
    public String edit(@PathVariable String id, Model model) {
        SysMenu sysMenu = sysMenuService.getById(id);
        model.addAttribute("sysMenu", sysMenu);
        if (sysMenu.getDeep() == 1) {
            return "/system/menu/edit";
        } else if (sysMenu.getDeep() == 2) {
            QueryWrapper<SysMenu> ew = new QueryWrapper<SysMenu>();
            ew.eq("pid", "0");
            ew.orderByAsc("sort");
            List<SysMenu> list = sysMenuService.list(ew);
            model.addAttribute("list", list);
            return "/system/menu/editMenu";
        } else {
            QueryWrapper<SysMenu> ew = new QueryWrapper<SysMenu>();
            ew.eq("pid", "0");
            ew.orderByAsc("sort");
            List<SysMenu> list = sysMenuService.list(ew);
            model.addAttribute("list", list);
            model.addAttribute("pSysMenu", sysMenuService.getById(sysMenu.getPid()));
            return "/system/menu/editAction";
        }
    }

    /**
     * 执行编辑菜单
     *
     * @param sysMenu
     * @param model
     * @return
     */
    @Log("编辑菜单")
    @RequiresPermissions("editMenu")
    @RequestMapping("/doEdit")
    @ResponseBody
    public AjaxResult doEdit(SysMenu sysMenu) {
        sysMenuService.updateById(sysMenu);
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return AjaxResult.success();
    }

    /**
     * 删除菜单
     *
     * @param id
     * @return
     */
    @Log("删除菜单")
    @RequiresPermissions("deleteMenu")
    @RequestMapping("/delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable String id) {
        sysMenuService.removeById(id);
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return AjaxResult.success();
    }

    /**
     * 根据父节点获取子菜单
     *
     * @param pid
     * @return
     */
    @RequestMapping("/json")
    @ResponseBody
    public AjaxResult json(String pid) {
        QueryWrapper<SysMenu> ew = new QueryWrapper<SysMenu>();
        ew.eq("pid", pid);
        ew.orderByAsc("sort");
        List<SysMenu> list = sysMenuService.list(ew);
        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
        for (SysMenu m : list) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", m.getId());
            map.put("text", m.getMenuName());
            listMap.add(map);
        }
        return AjaxResult.success(listMap);
    }

    /**
     * 验证菜单资源名称是否存在
     *
     * @param resource
     * @return
     */
    @RequestMapping("/checkMenuResource")
    @ResponseBody
    public AjaxResult checkMenuResource(String resource) {
        List<SysMenu> list = sysMenuService.list(new QueryWrapper<SysMenu>().eq("resource", resource));
        if (list.size() > 0) {
            return AjaxResult.error("资源已存在,请换一个尝试.");
        }
        return AjaxResult.success();
    }

}
