package com.hong.controller.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hong.common.AjaxResult;
import com.hong.common.controller.BaseController;
import com.hong.entity.SysRole;
import com.hong.entity.SysUser;
import com.hong.entity.SysUserRole;
import com.hong.service.ISysDictService;
import com.hong.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/system/myinfo")
public class MyInfoController extends BaseController {

    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ISysDictService sysDictService;

    /**
     * 修改密码页面
     *
     * @param
     * @return
     */
    @RequestMapping("/pwd")
    public String pwd() {
        return "system/myinfo/pwd";
    }

    /**
     * 个人信息页面
     *
     * @param
     * @return
     */
    @RequestMapping("/info")
    public String info(Model model) {
        SysUser sysUser = sysUserService.getById(getUserId());

        QueryWrapper<SysUserRole> ew = new QueryWrapper<SysUserRole>();
        ew.eq("user_id ", getUserId());
        model.addAttribute("sysUser", sysUser);
        model.addAttribute("sexDicts", sysDictService.selectSysDictList("sex_dict"));
        model.addAttribute("politicDicts", sysDictService.selectSysDictList("politic_dict"));
        return "system/myinfo/info";
    }

    /**
     * 修改密码
     *
     * @param password           旧密码
     * @param newpassword        新密码
     * @param newpassword2       再次输入新密码
     * @param redirectAttributes
     * @return
     */
    @RequestMapping("/doChangePwd")
    @ResponseBody
    public AjaxResult doChangePwd(String password, String newpassword, String newpassword2,
                                  RedirectAttributes redirectAttributes) {
        if (StringUtils.isBlank(password) || StringUtils.isBlank(newpassword) || StringUtils.isBlank(newpassword2)) {
            return AjaxResult.error("客户端提交数据不能为空.");
        }
        Subject subject = SecurityUtils.getSubject();
        SysUser sysUser = (SysUser) subject.getPrincipal();
        SysUser user = sysUserService.getById(sysUser.getId());
        if (!user.getPassword().equals(new SimpleHash("MD5", password, sysUser.getUserName(), 1024).toString())) {
            return AjaxResult.error("旧密码输入错误.");
        }
        if (!newpassword2.equals(newpassword)) {
            return AjaxResult.error("两次输入的密码不一致.");
        }
        user.setPassword(new SimpleHash("MD5", newpassword, sysUser.getUserName(), 1024).toString());
        sysUserService.updateById(user);
        return AjaxResult.success("密码修改成功.");
    }
}
