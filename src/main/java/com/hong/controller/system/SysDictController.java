package com.hong.controller.system;

import com.hong.common.AjaxResult;
import com.hong.common.anno.Log;
import com.hong.common.controller.BaseController;
import com.hong.common.util.Convert;
import com.hong.common.util.DateUtils;
import com.hong.common.util.excel.Excel;
import com.hong.common.util.excel.ExcelStyleEnum;
import com.hong.common.util.excel.ExcelUtils;
import com.hong.entity.SysDict;
import com.hong.service.ISysDictService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 字典数据Controller
 *
 * @author cuic
 * @date 2022-03-08
 */
@Controller
@RequestMapping("/system/dict")
public class SysDictController extends BaseController {
    private String prefix = "system/dict";
    @Autowired
    private ISysDictService sysDictService;

    @RequiresPermissions("dict:list")
    @GetMapping()
    public String dict() {
        return prefix + "/list";
    }

    /**
     * 查询字典数据列表
     */
    @RequiresPermissions("dict:list")
    @PostMapping("/list")
    @ResponseBody
    public AjaxResult list(SysDict sysDict) {
        startPage(sysDict);
        List<SysDict> list = sysDictService.selectSysDictList(sysDict);
        return getDataTable(list);
    }

    /**
     * 导出字典数据列表
     */
    @RequiresPermissions("dict:list")
    @Log("字典数据列表")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysDict sysDict) throws Exception {
        List<SysDict> list = sysDictService.selectSysDictList(sysDict);
        List<Map<String, Excel>> excelDate = new ArrayList<>();
        list.forEach(t -> {
            Map<String, Excel> row = new LinkedHashMap<>();
            //封装excel格式
            row.put("字典编码", new Excel(t.getDictCode(), ExcelStyleEnum.CENTER_FRAME));
            row.put("字典排序", new Excel(t.getDictSort(), ExcelStyleEnum.CENTER_FRAME));
            row.put("字典标签", new Excel(t.getDictLabel(), ExcelStyleEnum.CENTER_FRAME));
            row.put("字典键值", new Excel(t.getDictValue(), ExcelStyleEnum.CENTER_FRAME));
            row.put("字典类型", new Excel(t.getDictType(), ExcelStyleEnum.CENTER_FRAME));
            row.put("状态（0正常 1停用）", new Excel(t.getStatus(), ExcelStyleEnum.CENTER_FRAME));
            row.put("创建者", new Excel(t.getCreateBy(), ExcelStyleEnum.CENTER_FRAME));
            row.put("创建时间", new Excel(DateUtils.parseDateToYYYY_MM_DD(t.getCreateTime()), ExcelStyleEnum.CENTER_FRAME));
            row.put("更新者", new Excel(t.getUpdateBy(), ExcelStyleEnum.CENTER_FRAME));
            row.put("更新时间", new Excel(DateUtils.parseDateToYYYY_MM_DD(t.getUpdateTime()), ExcelStyleEnum.CENTER_FRAME));
            row.put("备注", new Excel(t.getRemark(), ExcelStyleEnum.CENTER_FRAME));

            excelDate.add(row);
        });

        HSSFWorkbook sheets = ExcelUtils.listToExcelAlone(excelDate);
        ExcelUtils.export(sheets, "字典数据.xls");
        return AjaxResult.success();
    }

    /**
     * 新增字典数据
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存字典数据
     */
    @RequiresPermissions("dict:add")
    @Log("新增字典数据")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysDict sysDict) {
        return toAjax(sysDictService.insertSysDict(sysDict));
    }

    /**
     * 修改字典数据
     */
    @GetMapping("/edit/{dictCode}")
    public String edit(@PathVariable("dictCode") Long dictCode, Model model) {
        SysDict sysDict = sysDictService.selectSysDictById(dictCode);
        model.addAttribute("data", sysDict);
        return prefix + "/edit";
    }

    /**
     * 查看字典数据
     */
    @RequiresPermissions("dict:detail")
    @GetMapping("/detail/{dictCode}")
    public String detail(@PathVariable("dictCode") Long dictCode, Model model) {
        SysDict sysDict = sysDictService.selectSysDictById(dictCode);
        model.addAttribute("data", sysDict);
        return prefix + "/detail";
    }

    /**
     * 修改保存字典数据
     */
    @RequiresPermissions("dict:edit")
    @Log("修改字典数据")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysDict sysDict) {
        return toAjax(sysDictService.updateSysDict(sysDict));
    }

    /**
     * 删除
     */
    @RequiresPermissions("dict:remove")
    @Log("删除字典数据")
    @PostMapping("/remove/{dictCode}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("dictCode") Long dictCode) {
        return toAjax(sysDictService.deleteSysDictById(dictCode));
    }

    /**
     * 导入字典数据
     */
    @RequiresPermissions("dict:add")
    @Log("导入字典数据")
    @GetMapping("/toImport")
    public String toImport() {
        return prefix + "/import";
    }

    /**
     * 导入字典数据
     */
    @RequiresPermissions("dict:add")
    @Log("导入字典数据")
    @PostMapping("/import")
    @ResponseBody
    public AjaxResult importFile(MultipartFile importFile) throws IOException {
        List<Map<String, String>> excelData = ExcelUtils.excelToArrayList(importFile);
        if (!CollectionUtils.isEmpty(excelData)) {
            List<SysDict> list = new ArrayList<>();
            excelData.forEach(t -> {
                //封装实体对象
                SysDict row = new SysDict();
                row.setDictCode(Convert.toLong(t.get("字典编码")));
                row.setDictSort(Convert.toInt(t.get("字典排序")));
                row.setDictLabel(t.get("字典标签"));
                row.setDictValue(t.get("字典键值"));
                row.setDictType(t.get("字典类型"));
                row.setStatus(t.get("状态（0正常 1停用）"));
                row.setCreateBy(t.get("创建者"));
                row.setCreateTime(DateUtils.parseDate(t.get("创建时间")));
                row.setUpdateBy(t.get("更新者"));
                row.setUpdateTime(DateUtils.parseDate(t.get("更新时间")));
                row.setRemark(t.get("备注"));
                list.add(row);
            });
            //入库

            list.forEach(x -> {
                sysDictService.insertSysDict(x);
            });

        }
        return AjaxResult.success();
    }
}
