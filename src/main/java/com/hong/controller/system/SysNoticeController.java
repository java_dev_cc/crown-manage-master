package com.hong.controller.system;

import com.hong.common.AjaxResult;
import com.hong.common.anno.Log;
import com.hong.common.controller.BaseController;
import com.hong.common.util.Convert;
import com.hong.common.util.DateUtils;
import com.hong.common.util.excel.ExcelUtils;
import com.hong.entity.SysNotice;
import com.hong.service.ISysDictService;
import com.hong.service.ISysNoticeService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 通知公告Controller
 *
 * @author cuic
 * @date 2022-03-28
 */
@Controller
@RequestMapping("/system/notice")
public class SysNoticeController extends BaseController {
    private String prefix = "system/notice";

    @Autowired
    private ISysNoticeService sysNoticeService;
    @Autowired
    private ISysDictService sysDictService;

    @RequiresPermissions("notice:list")
    @GetMapping()
    public String notice() {
        return prefix + "/list";
    }

    /**
     * 查询通知公告列表
     */
    @RequiresPermissions("notice:list")
    @PostMapping("/list")
    @ResponseBody
    public AjaxResult list(SysNotice sysNotice) {
        startPage(sysNotice);
        List<SysNotice> list = sysNoticeService.selectSysNoticeList(sysNotice);
        return getDataTable(list);
    }


    /**
     * 新增通知公告
     */
    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("noticeTypeDicts", sysDictService.selectSysDictList("notice_noticeType_dict"));
        return prefix + "/add";
    }

    /**
     * 新增保存通知公告
     */
    @RequiresPermissions("notice:add")
    @Log("新增通知公告")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysNotice sysNotice) {
        sysNotice.setCreateBy(getUserId());
        return toAjax(sysNoticeService.insertSysNotice(sysNotice));
    }

    /**
     * 修改通知公告
     */
    @GetMapping("/edit/{noticeId}")
    public String edit(@PathVariable("noticeId") Integer noticeId, Model model) {
        SysNotice sysNotice = sysNoticeService.selectSysNoticeById(noticeId);
        model.addAttribute("data", sysNotice);
        model.addAttribute("noticeTypeDicts", sysDictService.selectSysDictList("notice_noticeType_dict"));
        return prefix + "/edit";
    }

    /**
     * 查看通知公告
     */
    @GetMapping("/detail/{noticeId}")
    public String detail(@PathVariable("noticeId") Integer noticeId, Model model) {
        SysNotice sysNotice = sysNoticeService.selectSysNoticeById(noticeId);
        model.addAttribute("data", sysNotice);
        return prefix + "/detail";
    }

    /**
     * 修改保存通知公告
     */
    @RequiresPermissions("notice:edit")
    @Log("修改通知公告")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysNotice sysNotice) {
        sysNotice.setUpdateBy(getUserId());
        return toAjax(sysNoticeService.updateSysNotice(sysNotice));
    }

    /**
     * 删除
     */
    @RequiresPermissions("notice:delete")
    @Log("删除通知公告")
    @PostMapping("/remove/{noticeId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("noticeId") Integer noticeId) {
        return toAjax(sysNoticeService.deleteSysNoticeById(noticeId));
    }


}
