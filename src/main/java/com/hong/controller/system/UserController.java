package com.hong.controller.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hong.common.AjaxResult;
import com.hong.common.anno.Log;
import com.hong.common.controller.BaseController;
import com.hong.common.util.ShiroUtil;
import com.hong.entity.SysRole;
import com.hong.entity.SysUser;
import com.hong.entity.SysUserRole;
import com.hong.service.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/system/user")
public class UserController extends BaseController {

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISysRoleService sysRoleService;

    @Autowired
    private ISysUserRoleService sysUserRoleService;

    @Autowired
    private ISysDictService sysDictService;
    /**
     * 跳转用户管理
     *
     * @return
     */
    @RequiresPermissions("listUser")
    @RequestMapping("/listPage")
    public String listPage() {
        return "system/user/list";
    }

    /**
     * 分页查询用户
     *
     * @param search    检索条件
     * @param daterange 时间范围筛选
     * @param limit     每页条数
     * @param offset    偏移值
     * @return
     */
    @RequiresPermissions("listUser")
    @RequestMapping("/listUser")
    @ResponseBody
    public AjaxResult listUser(String search, String daterange, String userType, Integer limit, Integer offset) {
        PageHelper.startPage(getPageNo(limit, offset), limit);
        List<Map<Object, Object>> userList = sysUserService.selectUserPage(search, userType);
        PageInfo<Map<Object, Object>> pageData = new PageInfo<>(userList);
        AjaxResult resultVo = AjaxResult.success();
        resultVo.put("total", pageData.getTotal());
        resultVo.put("rows", pageData.getList());
        return resultVo;
    }

    /**
     * 跳转用户添加
     *
     * @param model
     * @return
     */
    @RequiresPermissions("addUser")
    @RequestMapping("/add")
    public String add(Model model) {
        model.addAttribute("roleList", sysRoleService.list(null));
        model.addAttribute("sexDicts",sysDictService.selectSysDictList("sex_dict"));
        model.addAttribute("politicDicts",sysDictService.selectSysDictList("politic_dict"));
        return "system/user/add";
    }

    /**
     * 执行用户新增
     *
     * @param user
     * @param roleId
     * @return
     */
    @Log("创建用户")
    @RequiresPermissions("addUser")
    @RequestMapping("/doAdd")
    @ResponseBody
    public AjaxResult doAdd(SysUser user,
                            @RequestParam(value = "roleId[]", required = false) String[] roleId,
                            @RequestParam(value = "pressIds[]", required = false) String[] pressIds,
                            @RequestParam(value = "typeIds[]", required = false) String[] typeIds) {

        int countUserName = sysUserService.selectCountByUserNameAndId(user.getUserName(), user.getId());
        if (countUserName > 0) {
            return AjaxResult.error("登录账号已存在");
        }

        int countUserDesc = sysUserService.selectCountByUserDescAndId(user.getUserDesc(), user.getId());
        if (countUserDesc > 0) {
            return AjaxResult.error("用户名称已存在");
        }

        sysUserService.insertUser(user, roleId, pressIds, typeIds);
        return AjaxResult.success();
    }

    /**
     * 编辑用户
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/edit/{id}")
    @RequiresPermissions("editUser")
    public String edit(@PathVariable String id, Model model) {
        SysUser sysUser = sysUserService.getById(id);
        List<SysRole> sysRoles = sysRoleService.list(null);
        QueryWrapper<SysUserRole> ew = new QueryWrapper<SysUserRole>();
        ew.eq("user_id ", id);
        List<SysUserRole> mySysUserRoles = sysUserRoleService.list(ew);
        List<String> myRolds = mySysUserRoles.stream().map(m -> m.getRoleId()).collect(Collectors.toList());
        model.addAttribute("sysUser", sysUser);
        model.addAttribute("sysRoles", sysRoles);
        model.addAttribute("myRolds", myRolds);
        model.addAttribute("sexDicts",sysDictService.selectSysDictList("sex_dict"));
        model.addAttribute("politicDicts",sysDictService.selectSysDictList("politic_dict"));
        return "system/user/edit";
    }

    /**
     * 执行编辑
     *
     * @param sysUser
     * @param roleId
     * @return
     */
    @Log("编辑用户")
    @RequestMapping("/doEdit")
    @ResponseBody
    public AjaxResult doEdit(SysUser sysUser,
                             @RequestParam(value = "roleId[]", required = false) String[] roleId,
                             @RequestParam(value = "pressIds[]", required = false) String[] pressIds,
                             @RequestParam(value = "typeIds[]", required = false) String[] typeIds) {
        int countUserName = sysUserService.selectCountByUserNameAndId(sysUser.getUserName(), sysUser.getId());
        if (countUserName > 0) {
            return AjaxResult.error("登录账号已存在");
        }

        int countUserDesc = sysUserService.selectCountByUserDescAndId(sysUser.getUserDesc(), sysUser.getId());
        if (countUserDesc > 0) {
            return AjaxResult.error("用户名称已存在");
        }
        sysUserService.updateUser(sysUser, roleId, pressIds, typeIds);
        return AjaxResult.success();
    }

    /**
     * 删除用户
     *
     * @param id
     * @return
     */
    @Log("删除用户")
    @RequiresPermissions("deleteUser")
    @RequestMapping("/delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable String id) {
        if (id.equals(getUserId())) {
            return AjaxResult.error("当前用户无法删除");
        }
        sysUserService.update(new UpdateWrapper<SysUser>().eq("id", id).set("user_state", "2"));

        return AjaxResult.success();
    }


    /**
     * 重置密码为123
     *
     * @param id
     * @return
     */
    @Log("重置密码")
    @RequestMapping("/reset/{id}")
    @ResponseBody
    public AjaxResult reset(@PathVariable String id) {
        if (id.equals(getUserId())) {
            return AjaxResult.error("当前用户密码无法重置");
        }
        SysUser user = sysUserService.getById(id);
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("id", id);
        updateWrapper.set("password", ShiroUtil.md51024Pwd("123", user.getUserName()));
        sysUserService.update(updateWrapper);
        return AjaxResult.success();
    }

    /**
     * 验证用户名是否存在
     *
     * @param userName
     * @return
     */
    @RequestMapping("/checkName")
    @ResponseBody
    public AjaxResult checkName(String userName) {
        List<SysUser> list = sysUserService.list(new QueryWrapper<SysUser>().eq("user_name", userName));
        if (list.size() > 0) {
            return AjaxResult.error("用户名已存在");
        }
        return AjaxResult.success();
    }

}
