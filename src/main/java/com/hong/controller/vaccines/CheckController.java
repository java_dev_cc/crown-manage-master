package com.hong.controller.vaccines;

import java.io.IOException;
import java.util.*;

import com.hong.entity.SysUser;
import com.hong.service.ISysUserService;
import org.apache.commons.collections.CollectionUtils;
import com.hong.service.ISysDictService;

import com.hong.common.util.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hong.common.anno.Log;
import com.hong.common.AjaxResult;
import com.hong.entity.Check;
import com.hong.service.ICheckService;
import com.hong.common.controller.BaseController;
import com.hong.common.AjaxResult;
import com.hong.common.util.excel.Excel;
import com.hong.common.util.excel.ExcelStyleEnum;
import com.hong.common.util.excel.ExcelUtils;
import org.springframework.web.multipart.MultipartFile;
import com.hong.common.util.Convert;

/**
 * 核酸检测记录Controller
 *
 * @author cuic
 * @date 2022-04-19
 */
@Controller
@RequestMapping("/vaccines/check")
public class CheckController extends BaseController {
    private String prefix = "vaccines/check";

    @Autowired
    private ICheckService checkService;
    @Autowired
    private ISysDictService sysDictService;
    @Autowired
    private ISysUserService userService;

    @RequiresPermissions("check:list")
    @GetMapping()
    public String check() {
        return prefix + "/list";
    }

    /**
     * 查询核酸检测记录列表
     */
    @RequiresPermissions("check:list")
    @PostMapping("/list")
    @ResponseBody
    public AjaxResult list(Check check) {

        if(1==getUserType()){
            check.setUserId(getUserId());
        }
        startPage(check);
        List<Check> list = checkService.selectCheckList(check);
        return getDataTable(list);
    }

    /**
     * 导出核酸检测记录列表
     */
    @RequiresPermissions("check:export")
    @Log("核酸检测记录列表")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Check check) throws Exception {
        List<Check> list = checkService.selectCheckList(check);
        List<Map<String, Excel>> excelDate = new ArrayList<>();
        list.forEach(t -> {
            Map<String, Excel> row = new LinkedHashMap<>();
            //封装excel格式
            row.put("id", new Excel(t.getId(), ExcelStyleEnum.CENTER_FRAME));
            row.put("用户id", new Excel(t.getUserId(), ExcelStyleEnum.CENTER_FRAME));
            row.put("检查结果", new Excel(t.getCheckResult(), ExcelStyleEnum.CENTER_FRAME));
            row.put("采样时间", new Excel(DateUtils.parseDateToYYYY_MM_DD(t.getCheckTime()), ExcelStyleEnum.CENTER_FRAME));
            row.put("采样点名称", new Excel(t.getCheckAddress(), ExcelStyleEnum.CENTER_FRAME));
            row.put("状态", new Excel(t.getStatus(), ExcelStyleEnum.CENTER_FRAME));
            row.put("证明图", new Excel(t.getEvidenceImg(), ExcelStyleEnum.CENTER_FRAME));
            row.put("创建者", new Excel(t.getCreateBy(), ExcelStyleEnum.CENTER_FRAME));
            row.put("创建时间", new Excel(DateUtils.parseDateToYYYY_MM_DD(t.getCreateTime()), ExcelStyleEnum.CENTER_FRAME));
            row.put("更新者", new Excel(t.getUpdateBy(), ExcelStyleEnum.CENTER_FRAME));
            row.put("更新时间", new Excel(DateUtils.parseDateToYYYY_MM_DD(t.getUpdateTime()), ExcelStyleEnum.CENTER_FRAME));
            row.put("备注", new Excel(t.getRemark(), ExcelStyleEnum.CENTER_FRAME));

            excelDate.add(row);
        });

        HSSFWorkbook sheets = ExcelUtils.listToExcelAlone(excelDate);
        ExcelUtils.export(sheets, "核酸检测记录.xls");
        return AjaxResult.success();
    }

    /**
     * 新增核酸检测记录
     */
    @GetMapping("/add")
    public String add(Model model) {

        List<SysUser> users;
        if (getUserType() == 1) {
            users = new ArrayList<>();
            users.add(getSysUser());
        } else {
            users = userService.selectByUserType("1");
        }
        model.addAttribute("userList", users);
        model.addAttribute("checkResultDicts", sysDictService.selectSysDictList("check_checkResult_dict"));
        return prefix + "/add";
    }

    /**
     * 新增保存核酸检测记录
     */
    @RequiresPermissions("check:add")
    @Log("新增核酸检测记录")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Check check) {
        return toAjax(checkService.insertCheck(check));
    }

    /**
     * 修改核酸检测记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        Check check = checkService.selectCheckById(id);
        List<SysUser> users;
        if (getUserType() == 1) {
            users = new ArrayList<>();
            users.add(getSysUser());
        } else {
            users = userService.selectByUserType("1");
        }
        model.addAttribute("userList", users);
        model.addAttribute("data", check);
        model.addAttribute("checkResultDicts", sysDictService.selectSysDictList("check_checkResult_dict"));
        return prefix + "/edit";
    }

    /**
     * 查看核酸检测记录
     */
    @RequiresPermissions("check:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, Model model) {
        Check check = checkService.selectCheckById(id);
        model.addAttribute("data", check);
        return prefix + "/detail";
    }

    /**
     * 修改保存核酸检测记录
     */
    @RequiresPermissions("check:edit")
    @Log("修改核酸检测记录")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Check check) {
        return toAjax(checkService.updateCheck(check));
    }

    /**
     * 删除
     */
    @RequiresPermissions("check:delete")
    @Log("删除核酸检测记录")
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id) {
        return toAjax(checkService.deleteCheckById(id));
    }

    /**
     * 导入核酸检测记录
     */
    @RequiresPermissions("check:add")
    @Log("导入核酸检测记录")
    @GetMapping("/toImport")
    public String toImport() {
        return prefix + "/import";
    }

    /**
     * 导入核酸检测记录
     */
    @RequiresPermissions("check:add")
    @Log("导入核酸检测记录")
    @PostMapping("/import")
    @ResponseBody
    public AjaxResult importFile(MultipartFile importFile) throws IOException {
        List<Map<String, String>> excelData = ExcelUtils.excelToArrayList(importFile);
        if (!CollectionUtils.isEmpty(excelData)) {
            List<Check> list = new ArrayList<>();
            excelData.forEach(t -> {
                //封装实体对象
                Check row = new Check();
                row.setId(Convert.toLong(t.get("id")));
                row.setUserId(t.get("用户id"));
                row.setCheckResult(t.get("检查结果"));
                row.setCheckTime(DateUtils.parseDate(t.get("采样时间")));
                row.setCheckAddress(t.get("采样点名称"));
                row.setStatus(t.get("状态"));
                row.setEvidenceImg(t.get("证明图"));
                row.setCreateBy(t.get("创建者"));
                row.setCreateTime(DateUtils.parseDate(t.get("创建时间")));
                row.setUpdateBy(t.get("更新者"));
                row.setUpdateTime(DateUtils.parseDate(t.get("更新时间")));
                row.setRemark(t.get("备注"));
                list.add(row);
            });
            //入库

            list.forEach(x -> {
                checkService.insertCheck(x);
            });

        }
        return AjaxResult.success();
    }

    /**
     * 饼图  统计接种次数
     */
    @GetMapping("/map")
    @ResponseBody
    public AjaxResult pie() {
        return AjaxResult.success(checkService.map(DateUtils.parseDateToStr("YYYY", new Date())));
    }

}
