package com.hong.controller.vaccines;

import java.io.IOException;
import java.util.*;

import com.hong.entity.SysUser;
import com.hong.service.ISysUserService;
import org.apache.commons.collections.CollectionUtils;
import com.hong.service.ISysDictService;

import com.hong.common.util.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hong.common.anno.Log;
import com.hong.common.AjaxResult;
import com.hong.entity.Vaccines;
import com.hong.service.IVaccinesService;
import com.hong.common.controller.BaseController;
import com.hong.common.AjaxResult;
import com.hong.common.util.excel.Excel;
import com.hong.common.util.excel.ExcelStyleEnum;
import com.hong.common.util.excel.ExcelUtils;
import org.springframework.web.multipart.MultipartFile;
import com.hong.common.util.Convert;

/**
 * 疫苗接种记录Controller
 *
 * @author cuic
 * @date 2022-04-19
 */
@Controller
@RequestMapping("/vaccines/vaccines")
public class VaccinesController extends BaseController {
    private String prefix = "vaccines/vaccines";

    @Autowired
    private IVaccinesService vaccinesService;
    @Autowired
    private ISysDictService sysDictService;
    @Autowired
    private ISysUserService userService;

    @RequiresPermissions("vaccines:list")
    @GetMapping()
    public String vaccines() {
        return prefix + "/list";
    }

    /**
     * 查询疫苗接种记录列表
     */
    @RequiresPermissions("vaccines:list")
    @PostMapping("/list")
    @ResponseBody
    public AjaxResult list(Vaccines vaccines) {
        startPage(vaccines);
        if (1 == getUserType()) {
            vaccines.setUserId(getUserId());
        }
        List<Vaccines> list = vaccinesService.selectVaccinesList(vaccines);
        return getDataTable(list);
    }

    /**
     * 导出疫苗接种记录列表
     */
    @RequiresPermissions("vaccines:export")
    @Log("疫苗接种记录列表")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Vaccines vaccines) throws Exception {
        List<Vaccines> list = vaccinesService.selectVaccinesList(vaccines);
        List<Map<String, Excel>> excelDate = new ArrayList<>();
        list.forEach(t -> {
            Map<String, Excel> row = new LinkedHashMap<>();
            //封装excel格式
            row.put("id", new Excel(t.getId(), ExcelStyleEnum.CENTER_FRAME));
            row.put("用户id", new Excel(t.getUserId(), ExcelStyleEnum.CENTER_FRAME));
            row.put("疫苗名称", new Excel(t.getVaccinesName(), ExcelStyleEnum.CENTER_FRAME));
            row.put("接种日期", new Excel(DateUtils.parseDateToYYYY_MM_DD(t.getVaccinesDate()), ExcelStyleEnum.CENTER_FRAME));
            row.put("接种单位", new Excel(t.getVaccinesAddress(), ExcelStyleEnum.CENTER_FRAME));
            row.put("状态", new Excel(t.getStatus(), ExcelStyleEnum.CENTER_FRAME));
            row.put("创建者", new Excel(t.getCreateBy(), ExcelStyleEnum.CENTER_FRAME));
            row.put("证明图", new Excel(t.getEvidenceImg(), ExcelStyleEnum.CENTER_FRAME));
            row.put("创建时间", new Excel(DateUtils.parseDateToYYYY_MM_DD(t.getCreateTime()), ExcelStyleEnum.CENTER_FRAME));
            row.put("更新者", new Excel(t.getUpdateBy(), ExcelStyleEnum.CENTER_FRAME));
            row.put("更新时间", new Excel(DateUtils.parseDateToYYYY_MM_DD(t.getUpdateTime()), ExcelStyleEnum.CENTER_FRAME));
            row.put("备注", new Excel(t.getRemark(), ExcelStyleEnum.CENTER_FRAME));

            excelDate.add(row);
        });

        HSSFWorkbook sheets = ExcelUtils.listToExcelAlone(excelDate);
        ExcelUtils.export(sheets, "疫苗接种记录.xls");
        return AjaxResult.success();
    }

    /**
     * 饼图  统计接种次数
     */
    @GetMapping("/pie")
    @ResponseBody
    public AjaxResult pie() {
        return AjaxResult.success(vaccinesService.pieProcess());
    }

    /**
     * 新增疫苗接种记录
     */
    @GetMapping("/add")
    public String add(Model model) {
        List<SysUser> users;
        if (getUserType() == 1) {
            users = new ArrayList<>();
            users.add(getSysUser());
        } else {
            users = userService.selectByUserType("1");
        }
        model.addAttribute("userList", users);
        model.addAttribute("vaccinesNameDicts", sysDictService.selectSysDictList("vaccines_vaccinesName_dict"));
        return prefix + "/add";
    }

    /**
     * 新增保存疫苗接种记录
     */
    @RequiresPermissions("vaccines:add")
    @Log("新增疫苗接种记录")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Vaccines vaccines) {
        return toAjax(vaccinesService.insertVaccines(vaccines));
    }

    /**
     * 修改疫苗接种记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        Vaccines vaccines = vaccinesService.selectVaccinesById(id);
        List<SysUser> users;
        if (getUserType() == 1) {
            users = new ArrayList<>();
            users.add(getSysUser());
        } else {
            users = userService.selectByUserType("1");
        }
        model.addAttribute("userList", users);
        model.addAttribute("data", vaccines);
        model.addAttribute("vaccinesNameDicts", sysDictService.selectSysDictList("vaccines_vaccinesName_dict"));
        return prefix + "/edit";
    }

    /**
     * 查看疫苗接种记录
     */
    @RequiresPermissions("vaccines:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, Model model) {
        Vaccines vaccines = vaccinesService.selectVaccinesById(id);
        model.addAttribute("data", vaccines);
        return prefix + "/detail";
    }

    /**
     * 修改保存疫苗接种记录
     */
    @RequiresPermissions("vaccines:edit")
    @Log("修改疫苗接种记录")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Vaccines vaccines) {
        return toAjax(vaccinesService.updateVaccines(vaccines));
    }

    /**
     * 删除
     */
    @RequiresPermissions("vaccines:delete")
    @Log("删除疫苗接种记录")
    @PostMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id) {
        return toAjax(vaccinesService.deleteVaccinesById(id));
    }

    /**
     * 导入疫苗接种记录
     */
    @RequiresPermissions("vaccines:add")
    @Log("导入疫苗接种记录")
    @GetMapping("/toImport")
    public String toImport() {
        return prefix + "/import";
    }

    /**
     * 导入疫苗接种记录
     */
    @RequiresPermissions("vaccines:add")
    @Log("导入疫苗接种记录")
    @PostMapping("/import")
    @ResponseBody
    public AjaxResult importFile(MultipartFile importFile) throws IOException {
        List<Map<String, String>> excelData = ExcelUtils.excelToArrayList(importFile);
        if (!CollectionUtils.isEmpty(excelData)) {
            List<Vaccines> list = new ArrayList<>();
            excelData.forEach(t -> {
                //封装实体对象
                Vaccines row = new Vaccines();
                row.setId(Convert.toLong(t.get("id")));
                row.setUserId(t.get("用户id"));
                row.setVaccinesName(t.get("疫苗名称"));
                row.setVaccinesDate(DateUtils.parseDate(t.get("接种日期")));
                row.setVaccinesAddress(t.get("接种单位"));
                row.setStatus(t.get("状态"));
                row.setCreateBy(t.get("创建者"));
                row.setEvidenceImg(t.get("证明图"));
                row.setCreateTime(DateUtils.parseDate(t.get("创建时间")));
                row.setUpdateBy(t.get("更新者"));
                row.setUpdateTime(DateUtils.parseDate(t.get("更新时间")));
                row.setRemark(t.get("备注"));
                list.add(row);
            });
            //入库

            list.forEach(x -> {
                vaccinesService.insertVaccines(x);
            });

        }
        return AjaxResult.success();
    }
}
