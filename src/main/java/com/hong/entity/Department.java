package com.hong.entity;

import com.hong.common.util.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 部门对象 department
 *
 * @author cuic
 * @date 2022-03-08
 */
public class Department extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 部门编号
     */
    private Long id;

    /**
     * 部门名称
     */
    private String depname;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setDepname(String depname) {
        this.depname = depname;
    }

    public String getDepname() {
        return depname;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("depname", getDepname())
                .toString();
    }
}
