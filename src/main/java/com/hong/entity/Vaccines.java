package com.hong.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.hong.common.util.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 疫苗接种记录对象 vaccines_list
 *
 * @author cuic
 * @date 2022-04-19
 */
public class Vaccines extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 疫苗名称
     */
    private String vaccinesName;

    /**
     * 接种日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date vaccinesDate;

    /**
     * 接种单位
     */
    private String vaccinesAddress;

    /**
     * 状态
     */
    private String status;

    /**
     * 证明图
     */
    private String evidenceImg;


    /**
     * 用户名称
     */
    private String userDesc;

    public String getUserDesc() {
        return userDesc;
    }

    public void setUserDesc(String userDesc) {
        this.userDesc = userDesc;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setVaccinesName(String vaccinesName) {
        this.vaccinesName = vaccinesName;
    }

    public String getVaccinesName() {
        return vaccinesName;
    }

    public void setVaccinesDate(Date vaccinesDate) {
        this.vaccinesDate = vaccinesDate;
    }

    public Date getVaccinesDate() {
        return vaccinesDate;
    }

    public void setVaccinesAddress(String vaccinesAddress) {
        this.vaccinesAddress = vaccinesAddress;
    }

    public String getVaccinesAddress() {
        return vaccinesAddress;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setEvidenceImg(String evidenceImg) {
        this.evidenceImg = evidenceImg;
    }

    public String getEvidenceImg() {
        return evidenceImg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("vaccinesName", getVaccinesName())
                .append("vaccinesDate", getVaccinesDate())
                .append("vaccinesAddress", getVaccinesAddress())
                .append("status", getStatus())
                .append("createBy", getCreateBy())
                .append("evidenceImg", getEvidenceImg())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
