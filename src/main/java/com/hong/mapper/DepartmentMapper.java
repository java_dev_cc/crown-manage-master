package com.hong.mapper;

import com.hong.entity.Department;

import java.util.List;

/**
 * 部门Mapper接口
 *
 * @author cuic
 * @date 2022-03-09
 */
public interface DepartmentMapper {
    /**
     * 查询部门
     *
     * @param id 部门ID
     * @return 部门
     */
    public Department selectDepartmentById(Long id);

    /**
     * 查询部门列表
     *
     * @param department 部门
     * @return 部门集合
     */
    public List<Department> selectDepartmentList(Department department);

    /**
     * 新增部门
     *
     * @param department 部门
     * @return 结果
     */
    public int insertDepartment(Department department);

    /**
     * 修改部门
     *
     * @param department 部门
     * @return 结果
     */
    public int updateDepartment(Department department);

    /**
     * 删除部门
     *
     * @param id 部门ID
     * @return 结果
     */
    public int deleteDepartmentById(Long id);

    /**
     * 批量删除部门
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDepartmentByIds(String[] ids);
}
