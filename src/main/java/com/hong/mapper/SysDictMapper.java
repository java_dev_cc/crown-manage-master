package com.hong.mapper;

import com.hong.entity.SysDict;

import java.util.List;

/**
 * 字典数据Mapper接口
 *
 * @author cuic
 * @date 2022-03-09
 */
public interface SysDictMapper {
    /**
     * 查询字典数据
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    public SysDict selectSysDictById(Long dictCode);

    /**
     * 查询字典数据列表
     *
     * @param sysDict 字典数据
     * @return 字典数据集合
     */
    public List<SysDict> selectSysDictList(SysDict sysDict);

    /**
     * 新增字典数据
     *
     * @param sysDict 字典数据
     * @return 结果
     */
    public int insertSysDict(SysDict sysDict);

    /**
     * 修改字典数据
     *
     * @param sysDict 字典数据
     * @return 结果
     */
    public int updateSysDict(SysDict sysDict);

    /**
     * 删除字典数据
     *
     * @param dictCode 字典数据ID
     * @return 结果
     */
    public int deleteSysDictById(Long dictCode);

    /**
     * 批量删除字典数据
     *
     * @param dictCodes 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysDictByIds(String[] dictCodes);
}
