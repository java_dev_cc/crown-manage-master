package com.hong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hong.entity.SysLog;

/**
 * 日志表 Mapper 接口
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}