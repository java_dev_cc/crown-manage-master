package com.hong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hong.entity.SysRole;

/**
 * SysRole 表数据库控制层接口
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}