package com.hong.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hong.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * SysUser 表数据库控制层接口
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<Map<Object, Object>> selectUserList(@Param("search") String search, @Param("userType") String userType);

    int selectCountByUserNameAndId(@Param("userName") String userName, @Param("id") String id);

    int selectCountByUserDescAndId(@Param("userDesc") String userDesc, @Param("id") String id);
}
