package com.hong.service;

import java.util.List;

import com.hong.entity.Check;

/**
 * 核酸检测记录Service接口
 *
 * @author cuic
 * @date 2022-04-19
 */
public interface ICheckService {
    /**
     * 查询核酸检测记录
     *
     * @param id 核酸检测记录ID
     * @return 核酸检测记录
     */
    public Check selectCheckById(Long id);

    /**
     * 查询核酸检测记录列表
     *
     * @param check 核酸检测记录
     * @return 核酸检测记录集合
     */
    public List<Check> selectCheckList(Check check);

    /**
     * 新增核酸检测记录
     *
     * @param check 核酸检测记录
     * @return 结果
     */
    public int insertCheck(Check check);

    /**
     * 修改核酸检测记录
     *
     * @param check 核酸检测记录
     * @return 结果
     */
    public int updateCheck(Check check);

    /**
     * 批量删除核酸检测记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCheckByIds(String ids);

    /**
     * 删除核酸检测记录信息
     *
     * @param id 核酸检测记录ID
     * @return 结果
     */
    public int deleteCheckById(Long id);

    /**
     * 条形图  统计核酸检测次数
     */
    Integer[] map(String year);
}
