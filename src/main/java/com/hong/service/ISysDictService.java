package com.hong.service;

import com.hong.entity.SysDict;

import java.util.List;

/**
 * 字典数据Service接口
 *
 * @author cuic
 * @date 2022-03-09
 */
public interface ISysDictService {
    /**
     * 查询字典数据
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    public SysDict selectSysDictById(Long dictCode);

    /**
     * 查询字典数据列表
     *
     * @param sysDict 字典数据
     * @return 字典数据集合
     */
    public List<SysDict> selectSysDictList(SysDict sysDict);

    List<SysDict> selectSysDictList(String dictType);

    /**
     * 新增字典数据
     *
     * @param sysDict 字典数据
     * @return 结果
     */
    public int insertSysDict(SysDict sysDict);

    /**
     * 修改字典数据
     *
     * @param sysDict 字典数据
     * @return 结果
     */
    public int updateSysDict(SysDict sysDict);

    /**
     * 批量删除字典数据
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysDictByIds(String ids);

    /**
     * 删除字典数据信息
     *
     * @param dictCode 字典数据ID
     * @return 结果
     */
    public int deleteSysDictById(Long dictCode);
}
