package com.hong.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hong.entity.SysRole;

import java.util.List;

/**
 * SysRole 表数据服务层接口
 */
public interface ISysRoleService extends IService<SysRole> {

    boolean deleteByIds(List<String> roleIds);

}
