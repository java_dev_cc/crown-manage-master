package com.hong.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hong.entity.SysUser;

import java.util.List;
import java.util.Map;

/**
 * SysUser 表数据服务层接口
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 分页查询用户
     */
    List<Map<Object, Object>> selectUserPage(String search, String userType);

    /**
     * 保存用户
     */
    void insertUser(SysUser user, String... roleId);

    void insertUser(SysUser user, String[] roleId, String[] pressIds, String[] typeIds);

    /**
     * 更新用户
     */
    void updateUser(SysUser sysUser, String[] roleId, String[] pressIds, String[] typeIds);

    /**
     * 删除用户
     */
    void delete(String id);

    int selectCountByUserNameAndId(String userName, String id);

    int selectCountByUserDescAndId(String userDesc, String id);

    List<SysUser> selectByUserType(String userType);
}
