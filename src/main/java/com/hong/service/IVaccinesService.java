package com.hong.service;

import java.util.List;

import com.hong.entity.Vaccines;
import com.hong.entity.vo.PieMapVo;

/**
 * 疫苗接种记录Service接口
 *
 * @author cuic
 * @date 2022-04-19
 */
public interface IVaccinesService {
    /**
     * 查询疫苗接种记录
     *
     * @param id 疫苗接种记录ID
     * @return 疫苗接种记录
     */
    public Vaccines selectVaccinesById(Long id);

    /**
     * 查询疫苗接种记录列表
     *
     * @param vaccines 疫苗接种记录
     * @return 疫苗接种记录集合
     */
    public List<Vaccines> selectVaccinesList(Vaccines vaccines);

    /**
     * 新增疫苗接种记录
     *
     * @param vaccines 疫苗接种记录
     * @return 结果
     */
    public int insertVaccines(Vaccines vaccines);

    /**
     * 修改疫苗接种记录
     *
     * @param vaccines 疫苗接种记录
     * @return 结果
     */
    public int updateVaccines(Vaccines vaccines);

    /**
     * 批量删除疫苗接种记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteVaccinesByIds(String ids);

    /**
     * 删除疫苗接种记录信息
     *
     * @param id 疫苗接种记录ID
     * @return 结果
     */
    public int deleteVaccinesById(Long id);
    /**
     * 饼图  统计接种次数
     */
    List<PieMapVo>  pieProcess();
}
