package com.hong.service.impl;

import java.util.List;
import java.util.Map;

import com.hong.common.util.StringUtils;
import com.hong.common.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hong.mapper.CheckMapper;
import com.hong.entity.Check;
import com.hong.service.ICheckService;
import com.hong.common.util.Convert;

/**
 * 核酸检测记录Service业务层处理
 *
 * @author cuic
 * @date 2022-04-19
 */
@Service
public class CheckServiceImpl implements ICheckService {
    @Autowired
    private CheckMapper checkMapper;

    /**
     * 查询核酸检测记录
     *
     * @param id 核酸检测记录ID
     * @return 核酸检测记录
     */
    @Override
    public Check selectCheckById(Long id) {
        return checkMapper.selectCheckById(id);
    }

    /**
     * 查询核酸检测记录列表
     *
     * @param check 核酸检测记录
     * @return 核酸检测记录
     */
    @Override
    public List<Check> selectCheckList(Check check) {
        Map<String, Object> params = check.getParams();
        String dataRangeCheckTime = (String) params.get("dataRangeCheckTime");
        if (!StringUtils.isEmpty(dataRangeCheckTime)) {
            params.put("beginCheckTime", dataRangeCheckTime.split("~")[0].trim());
            params.put("endCheckTime", dataRangeCheckTime.split("~")[1].trim());
        }
        return checkMapper.selectCheckList(check);
    }

    /**
     * 新增核酸检测记录
     *
     * @param check 核酸检测记录
     * @return 结果
     */
    @Override
    public int insertCheck(Check check) {


        check.setCreateTime(DateUtils.getNowDate());
        return checkMapper.insertCheck(check);
    }

    /**
     * 修改核酸检测记录
     *
     * @param check 核酸检测记录
     * @return 结果
     */
    @Override
    public int updateCheck(Check check) {
        check.setUpdateTime(DateUtils.getNowDate());
        return checkMapper.updateCheck(check);
    }

    /**
     * 删除核酸检测记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCheckByIds(String ids) {
        return checkMapper.deleteCheckByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除核酸检测记录信息
     *
     * @param id 核酸检测记录ID
     * @return 结果
     */
    @Override
    public int deleteCheckById(Long id) {
        return checkMapper.deleteCheckById(id);
    }
    /**
     * 条形图  统计核酸检测次数
     */
    @Override
    public Integer[] map(String year) {
        return checkMapper.map(year);
    }
}
