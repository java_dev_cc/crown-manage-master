package com.hong.service.impl;

import com.hong.common.util.Convert;
import com.hong.common.util.DateUtils;
import com.hong.common.util.StringUtils;
import com.hong.entity.SysDict;
import com.hong.mapper.SysDictMapper;
import com.hong.service.ISysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典数据Service业务层处理
 *
 * @author cuic
 * @date 2022-03-09
 */
@Service
public class SysDictServiceImpl implements ISysDictService {
    @Autowired
    private SysDictMapper sysDictMapper;

    /**
     * 查询字典数据
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    public SysDict selectSysDictById(Long dictCode) {
        return sysDictMapper.selectSysDictById(dictCode);
    }

    /**
     * 查询字典数据列表
     *
     * @param sysDict 字典数据
     * @return 字典数据
     */
    @Override
    public List<SysDict> selectSysDictList(SysDict sysDict) {
        return sysDictMapper.selectSysDictList(sysDict);
    }

    /**
     * 查询字典数据列表
     *
     * @param dictType 字典类型
     * @return 字典数据
     */
    @Override
    public List<SysDict> selectSysDictList(String dictType) {
        SysDict sysDict = new SysDict();
        sysDict.setDictType(dictType);
        return sysDictMapper.selectSysDictList(sysDict);
    }

    /**
     * 新增字典数据
     *
     * @param sysDict 字典数据
     * @return 结果
     */
    @Override
    public int insertSysDict(SysDict sysDict) {

        if (!StringUtils.isNull(sysDict.getDictCode())) {
            return this.updateSysDict(sysDict);
        }


        sysDict.setCreateTime(DateUtils.getNowDate());
        return sysDictMapper.insertSysDict(sysDict);
    }

    /**
     * 修改字典数据
     *
     * @param sysDict 字典数据
     * @return 结果
     */
    @Override
    public int updateSysDict(SysDict sysDict) {
        sysDict.setUpdateTime(DateUtils.getNowDate());
        return sysDictMapper.updateSysDict(sysDict);
    }

    /**
     * 删除字典数据对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysDictByIds(String ids) {
        return sysDictMapper.deleteSysDictByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除字典数据信息
     *
     * @param dictCode 字典数据ID
     * @return 结果
     */
    @Override
    public int deleteSysDictById(Long dictCode) {
        return sysDictMapper.deleteSysDictById(dictCode);
    }
}
