package com.hong.service.impl;

import com.hong.common.util.Convert;
import com.hong.common.util.DateUtils;
import com.hong.common.util.StringUtils;
import com.hong.entity.SysNotice;
import com.hong.mapper.SysNoticeMapper;
import com.hong.service.ISysNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通知公告Service业务层处理
 *
 * @author cuic
 * @date 2022-03-28
 */
@Service
public class SysNoticeServiceImpl implements ISysNoticeService {
    @Autowired
    private SysNoticeMapper sysNoticeMapper;

    /**
     * 查询通知公告
     *
     * @param noticeId 通知公告ID
     * @return 通知公告
     */
    @Override
    public SysNotice selectSysNoticeById(Integer noticeId) {
        return sysNoticeMapper.selectSysNoticeById(noticeId);
    }

    /**
     * 查询通知公告列表
     *
     * @param sysNotice 通知公告
     * @return 通知公告
     */
    @Override
    public List<SysNotice> selectSysNoticeList(SysNotice sysNotice) {
        return sysNoticeMapper.selectSysNoticeList(sysNotice);
    }

    /**
     * 新增通知公告
     *
     * @param sysNotice 通知公告
     * @return 结果
     */
    @Override
    public int insertSysNotice(SysNotice sysNotice) {

        if (!StringUtils.isNull(sysNotice.getNoticeId())) {
            return this.updateSysNotice(sysNotice);
        }


        sysNotice.setCreateTime(DateUtils.getNowDate());
        return sysNoticeMapper.insertSysNotice(sysNotice);
    }

    /**
     * 修改通知公告
     *
     * @param sysNotice 通知公告
     * @return 结果
     */
    @Override
    public int updateSysNotice(SysNotice sysNotice) {
        sysNotice.setUpdateTime(DateUtils.getNowDate());
        return sysNoticeMapper.updateSysNotice(sysNotice);
    }

    /**
     * 删除通知公告对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysNoticeByIds(String ids) {
        return sysNoticeMapper.deleteSysNoticeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除通知公告信息
     *
     * @param noticeId 通知公告ID
     * @return 结果
     */
    @Override
    public int deleteSysNoticeById(Integer noticeId) {
        return sysNoticeMapper.deleteSysNoticeById(noticeId);
    }
}
