package com.hong.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hong.common.util.ShiroUtil;
import com.hong.entity.SysUser;
import com.hong.entity.SysUserRole;
import com.hong.mapper.SysUserMapper;
import com.hong.mapper.SysUserRoleMapper;
import com.hong.service.ISysUserService;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * SysUser 表数据服务层接口实现类
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private SysUserRoleMapper userRoleMapper;


    @Override
    public void insertUser(SysUser user, String... roleIds) {
        insertUser(user, roleIds, null, null);
    }

    @Override
    public void insertUser(SysUser user, String[] roleIds, String[] pressIds, String[] typeIds) {
        user.setCreateTime(new Date());
        user.setPassword(ShiroUtil.md51024Pwd(user.getPassword(), user.getUserName()));
        user.setUserType(Integer.valueOf(roleIds[0]));
        // 保存用户
        userMapper.insert(user);
        insertRoleAndOther(user.getId(), roleIds, pressIds, typeIds);
    }

    private void insertRoleAndOther(String userId, String[] roleIds, String[] pressIds, String[] typeIds) {
        // 绑定角色
        if (ArrayUtils.isNotEmpty(roleIds)) {
            userRoleMapper.delete(new QueryWrapper<SysUserRole>().eq("user_id", userId));
            for (String rid : roleIds) {
                SysUserRole sysUserRole = new SysUserRole();
                sysUserRole.setUserId(userId);
                sysUserRole.setRoleId(rid);
                userRoleMapper.insert(sysUserRole);
            }
        }

    }

    @Override
    public void updateUser(SysUser user, String[] roleIds, String[] pressIds, String[] typeIds) {
        user.setPassword(null);
        // 更新用户
        userMapper.updateById(user);
        insertRoleAndOther(user.getId(), roleIds, pressIds, typeIds);
    }

    @Override
    public List<Map<Object, Object>> selectUserPage(String search, String userType) {
        return baseMapper.selectUserList(search, userType);
    }

    @Override
    @Transactional
    public void delete(String id) {
        // 删除用户绑定的角色
        userRoleMapper.delete(new QueryWrapper<SysUserRole>().eq("user_id", id));
        // 删除用户信息
        this.removeById(id);
    }

    @Override
    public int selectCountByUserNameAndId(String userName, String id) {
        return userMapper.selectCountByUserNameAndId(userName, id);
    }

    @Override
    public int selectCountByUserDescAndId(String userDesc, String id) {
        return userMapper.selectCountByUserDescAndId(userDesc, id);
    }

    @Override
    public List<SysUser> selectByUserType(String userType) {
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_type", userType).eq("user_state", 1);
        return userMapper.selectList(queryWrapper);
    }

}
