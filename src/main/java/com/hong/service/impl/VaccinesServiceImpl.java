package com.hong.service.impl;

import java.util.List;
import java.util.Map;

import com.hong.common.util.StringUtils;
import com.hong.common.util.DateUtils;
import com.hong.entity.vo.PieMapVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hong.mapper.VaccinesMapper;
import com.hong.entity.Vaccines;
import com.hong.service.IVaccinesService;
import com.hong.common.util.Convert;

/**
 * 疫苗接种记录Service业务层处理
 *
 * @author cuic
 * @date 2022-04-19
 */
@Service
public class VaccinesServiceImpl implements IVaccinesService {
    @Autowired
    private VaccinesMapper vaccinesMapper;

    /**
     * 查询疫苗接种记录
     *
     * @param id 疫苗接种记录ID
     * @return 疫苗接种记录
     */
    @Override
    public Vaccines selectVaccinesById(Long id) {
        return vaccinesMapper.selectVaccinesById(id);
    }

    /**
     * 查询疫苗接种记录列表
     *
     * @param vaccines 疫苗接种记录
     * @return 疫苗接种记录
     */
    @Override
    public List<Vaccines> selectVaccinesList(Vaccines vaccines) {
        Map<String, Object> params = vaccines.getParams();
        String dataRangeVaccinesDate = (String) params.get("dataRangeVaccinesDate");
        if (!StringUtils.isEmpty(dataRangeVaccinesDate)) {
            params.put("beginVaccinesDate", dataRangeVaccinesDate.split("~")[0].trim());
            params.put("endVaccinesDate", dataRangeVaccinesDate.split("~")[1].trim());
        }
        return vaccinesMapper.selectVaccinesList(vaccines);
    }

    /**
     * 新增疫苗接种记录
     *
     * @param vaccines 疫苗接种记录
     * @return 结果
     */
    @Override
    public int insertVaccines(Vaccines vaccines) {


        vaccines.setCreateTime(DateUtils.getNowDate());
        return vaccinesMapper.insertVaccines(vaccines);
    }

    /**
     * 修改疫苗接种记录
     *
     * @param vaccines 疫苗接种记录
     * @return 结果
     */
    @Override
    public int updateVaccines(Vaccines vaccines) {
        vaccines.setUpdateTime(DateUtils.getNowDate());
        return vaccinesMapper.updateVaccines(vaccines);
    }

    /**
     * 删除疫苗接种记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteVaccinesByIds(String ids) {
        return vaccinesMapper.deleteVaccinesByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除疫苗接种记录信息
     *
     * @param id 疫苗接种记录ID
     * @return 结果
     */
    @Override
    public int deleteVaccinesById(Long id) {
        return vaccinesMapper.deleteVaccinesById(id);
    }
    /**
     * 饼图  统计接种次数
     */
    @Override
    public List<PieMapVo> pieProcess() {
        return  vaccinesMapper.pieProcess();
    }
}
