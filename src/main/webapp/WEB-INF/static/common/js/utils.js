
/**
* 获取输入框的值
* **/
function getValue(obj) {
    return isEmpty(obj) ? null : obj;
}

/**
* 判断字符串是否为空
* **/
function isEmpty(value) {
    return value == null || this.trim(value) == "";

}

 function formToJson(data) {
    data=data.replace(/&/g,"\",\"");
    data=data.replace(/=/g,"\":\"");
    data="{\""+data+"\"}";
    return data;
}
